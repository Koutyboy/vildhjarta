#ifndef _VILDHJARTA_STATE_HPP
#define _VILDHJARTA_STATE_HPP

#include <SFML\Graphics.hpp>
#include "Context.hpp"

namespace vh
{
	class State
	{
	public:
		virtual void handleEvent(sf::Event& event, sf::RenderWindow& window)=0;
		virtual void update(float dt, float time, vh::Context& context)=0;
		virtual void render(sf::RenderWindow& window)=0;
		
		bool finished();
		bool remove();
		State* next();
		
		State()
		{
			m_next = nullptr;
			m_finished = false;
			m_remove = true;
		}
		
		virtual ~State() {};
	protected:
		bool m_finished;
		bool m_remove;
		State* m_next;
	};
}


#endif