#ifndef _VILDHJARTA_RESOURCECACHE_HPP
#define _VILDHJARTA_RESOURCECACHE_HPP

#include <map>
#include <string>

namespace vh
{
	template<class T>
	class ResourceCache
	{
	public:
		void remove(std::string resource)
		{
			class std::map< std::string, T* >::iterator it = m_cache.find(resource);
			if( it == m_cache.end() )
				return;
			delete (*it).second;
			m_cache.erase(it);
		}
		
		void add(std::string resource, T* t)
		{
			if( m_cache.find(resource) == m_cache.end() )
				return;
			
			m_cache[resource] = t;
		}
		
		void clear()
		{
			class std::map< std::string, T* >::iterator it = m_cache.begin();
			for(; it != m_cache.end(); ++it )
			{
				delete (*it).second;
			}
			m_cache.clear();
		}
		
		bool contains(std::string resource)
		{
			if( m_cache.find(resource) != m_cache.end() )
			{
				return true;
			}
			return false;
		}
		
		T* get(std::string resource)
		{
			class std::map< std::string, T* >::iterator it = m_cache.find(resource);
			if( it != m_cache.end() )
			{
				return (*it).second;
			}
			return nullptr;
		}
		
		~ResourceCache()
		{
			class std::map< std::string, T* >::iterator it = m_cache.begin();
			for(; it != m_cache.end(); ++it )
			{
				delete (*it).second;
			}
			m_cache.clear();
		}
	private:
		std::map< std::string, T* > m_cache;
	};
}

#endif