#include "State.hpp"

vh::State* vh::State::next()
{
	return m_next;
}

bool vh::State::finished()
{
	return m_finished;
}

bool vh::State::remove()
{
	return m_remove;
}