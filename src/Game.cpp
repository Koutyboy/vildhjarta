#include "Game.hpp"

int vh::GameWindow::exec()
{
	initialize();
	
	float dt = 1.0f / 60.0f;
	float nextUpdate = 0.0f;
	float lastUpdate = 0.0f;
	float currentTime = 0.0f;
	unsigned int maxSkipping = 5;
	unsigned int updatesInRow = 0;

	m_gameClock.restart();
	while( m_wind.isOpen() )
	{		
		while( m_wind.pollEvent( m_event ) )
		{
			handleEvent( m_event, m_wind );
		}
		
		currentTime = m_gameClock.getElapsedTime().asSeconds();
		updatesInRow = 0;
		while( updatesInRow < maxSkipping && currentTime >= nextUpdate)
		{
			float delta = currentTime - lastUpdate;
			update(delta, currentTime, m_context);
			render(m_wind);
			nextUpdate += dt;
			lastUpdate = currentTime;
			updatesInRow++;
		}
	}
	return 0;
}

bool vh::GameWindow::initialize()
{
	m_wind.create(sf::VideoMode(800,600,32), "@GameWindow", sf::Style::Titlebar);
}

void vh::GameWindow::handleEvent(sf::Event& event, sf::RenderWindow& window)
{
	if( event.key.code == sf::Keyboard::Escape )
		window.close();
}

void vh::GameWindow::update(float dt, float time, Context& context)
{
	
}

void vh::GameWindow::render(sf::RenderWindow& window)
{
	window.clear(sf::Color::Black);
	
	window.display();
}