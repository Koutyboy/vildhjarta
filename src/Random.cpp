#include "Random.hpp"
#include <stdlib.h>
#include <time.h>

int vh::util::Random::getIntiger(int min, int max)
{
	static bool initialized = false;
	if( !initialized )
	{
		srand(time(0));
		initialized = true;
	}
	return ( rand() % max ) + min;
}