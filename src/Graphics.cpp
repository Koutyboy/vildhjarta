#include "Graphics.hpp"
#include <SFML\Graphics.hpp>

sf::Texture* vh::Graphics::getTexture(std::string gfx)
{
	sf::Texture* texture = m_cache.get(gfx);
		
	if( texture == nullptr )
	{
		texture = new sf::Texture();
		if( !texture->loadFromFile(gfx) )
		{
			delete texture;
			return nullptr;
		}
		m_cache.add(gfx, texture);
	}
	return texture;
}

void vh::Graphics::loadTexture(std::string gfx)
{
	if( m_cache.contains(gfx) )
		return;
		
	sf::Texture* texture = new sf::Texture();
	if( !texture->loadFromFile(gfx) )
	{
		delete texture;
		return;
	}
	m_cache.add(gfx, texture);
}

void vh::Graphics::removeTexture(std::string gfx)
{
	m_cache.remove(gfx);
}