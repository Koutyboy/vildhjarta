#ifndef _VILDHJARTA_GRAPHICS_HPP
#define _VILDHJARTA_GRAPHICS_HPP

#include <SFML\Graphics.hpp>
#include "ResourceCache.hpp"
#include <string>
#include <map>
namespace vh
{
	class Graphics
	{
	public:
		sf::Texture* getTexture(std::string gfx);
		void removeTexture(std::string gfx);
		void loadTexture(std::string gfx);
	private:
		vh::ResourceCache<sf::Texture> m_cache;
	};
}



#endif