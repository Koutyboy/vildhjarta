#ifndef _VILDHJARTA_CONTEXT_HPP
#define _VILDHJARTA_CONTEXT_HPP

#include "Audio.hpp"
#include "Graphics.hpp"

namespace vh
{
	struct Context
	{
		Audio* audio;
		Graphics* graphics;
	};
}

#endif