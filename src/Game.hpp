#ifndef _VILDHJARTA_GAME_HPP
#define _VILDHJARTA_GAME_HPP

#include <SFML\Graphics.hpp>
#include "Context.hpp"
namespace vh
{
	class GameWindow
	{
	public:
		virtual int exec();
		
		GameWindow(){};
		virtual ~GameWindow() {};
	protected:
		virtual bool initialize();
		virtual void handleEvent(sf::Event& event, sf::RenderWindow& window);
		virtual void update(float dt, float time, Context& context);
		virtual void render(sf::RenderWindow& window);
		
		sf::RenderWindow m_wind;
		sf::Event m_event;
		sf::Clock m_gameClock;
		Context m_context;
	};
}

#endif