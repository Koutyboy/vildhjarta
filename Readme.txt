#########################################
# Vildhjarta - Game Programming Library #
#       Using - C++ and SFML 2.0        #
#					#
#           Version - 0.0.1 		#
#		Alpha			#
#########################################


#########################################
#            Information                #
#########################################
'Vildhjarta' is a library containing a collection of classes designed to improve development speed.
It was specifically designed to help under compos and jams.


#########################################
#              Important                #
#########################################
Both SFML and Vildhjarta need to be linked the same way, either both statically or dynamic.
Do not try to mix dynamic and static builds of the both libraries, this will result in the library not functioning
properly.

#########################################
#             Dependencies              #
#########################################
Vildhjarta utilizes the C++11 standard and builds upon SFML 2.0, it requires both to be available.

#########################################
#             Compiling                 #
#########################################
